

export const dimensions = {
    'posLeft': "0",
    'width': "300",
    "posTop": "0",
    "height": "300"
}
const diameter=15;

export var lastX = 0, lastY = 0, begin = false, reverseX = false, reverseY = false;
var arr = [0,0];

export const randomxy = (start) => {
    

    if (start === true) {
        arr = [];
        if (!begin) {
            lastX = dimensions.posLeft + calculateXDelta();
            lastY = dimensions.posTop + calculateYDelta();
            arr.push(checkType(lastX));
            arr.push(checkType(lastY));
            begin = true;
        }
        else {
            controlHorisontalMovement();
            controlVerticalMovement();
            arr.push(checkType(lastX));
            arr.push(checkType(lastY));
        }

    }

    function checkType(ar) {
        if (typeof ar=='string') { return parseInt(ar);}
        return ar;
    }

    function updateLastPos(ar1, ar2, char) {
        let a1=0, a2=0,res=0;
        if (typeof ar1 == "string") a1 = parseInt(ar1); else a1 = ar1;
        if (typeof ar2 == "string") a2 = parseInt(ar2); else a2 = ar2;
        if (char === "plus") res=a1 + a2
        else if (char === 'minus') res= a1 - a2;
        return res;
    }


    function controlHorisontalMovement() {
        if (!reverseX) {
            lastX=updateLastPos(lastX, calculateXDelta(), "plus");
            if (checkBallXPosition()) {
                lastX = parseInt(dimensions.width)-diameter;
                reverseX = true;
            }
        }
        else {
            lastX=updateLastPos(lastX, calculateXDelta(), "minus");
            if (checkBallXPosition()) {
                lastX = parseInt(dimensions.posLeft);
                reverseX = false;
            }

        }
    }

    function controlVerticalMovement() {
        if (!reverseY) {
            lastY=updateLastPos(lastY,calculateYDelta(),"plus");
            if (checkBallYPosition()) {
                lastY = parseInt(dimensions.height)-diameter;
                reverseY = true;
            }

        }
        else {
            lastY=updateLastPos(lastY,calculateYDelta(),"minus");
            if (checkBallYPosition()) {
                lastY = parseInt(dimensions.posTop);
                reverseY = false;
            }

        }
    }

    function checkBallXPosition() {
        if (!reverseX) {
            let ltd=parseInt(dimensions.width)-diameter;
            if (parseInt(lastX) >= (ltd)) return true; else return false;
        }
        else {
            if (parseInt(lastX) <= parseInt(dimensions.posLeft)) return true; else return false;
        }
    }

    function checkBallYPosition() {
        if (!reverseY) {
            let ltd=parseInt(dimensions.height)-diameter;
            if (parseInt(lastY) >= (ltd)) return true; else return false;
        }
        else {
            if (parseInt(lastY) <= parseInt(dimensions.posTop)) return true; else return false;
        }
    }


    function calculateXDelta() {
        let result = Math.floor((Math.random() * (parseInt(dimensions.width) - parseInt(dimensions.posLeft))/10) + parseInt(dimensions.posLeft));
        return result;
    }

    function calculateYDelta() {
        let result = Math.floor(Math.random() * (parseInt(dimensions.height) - parseInt(dimensions.posTop))/20 + parseInt(dimensions.posTop));
        return result;
    }

    return arr;

}






