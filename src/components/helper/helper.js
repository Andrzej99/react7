import "./../../styles/styles.css";

const ShowVariables = ({ children }) => {

    return (
        <div className="showVariables">
            {/* { console.log("children", { children })} */}
            <p>Xact={children[0]}</p>
            <p>Yact={children[1]}</p>
            <p>X0.current={(children[2])}</p>
            <p>Y0.current={children[3]}</p>
            <p>reverse={children[4]}</p>
            <p>start={children[5]}</p>
            <p>prevColor={children[6]}</p>
            <p>kolor={children[7]}</p>
        </div>
    )

}

export default ShowVariables;