
import "./../../styles/styles.css";
import styled from 'styled-components';
import { randomxy } from './../randomxy/randomxy.js';
import { useEffect, useState, useRef, useCallback } from 'react';
import { randomColor } from "./../randomColor/randomColor.js";



const BallDef = styled.div.attrs(props => ({
    bcak: props.bck,
}))`
 position:absolute;
 left:${props => props.left + 'px'};
 top:${props => props.top + 'px'};
width:15px;
height:15px;
background-color:${props => props.bcak};
border-radius:50%;
`;



const Ball = () => {
    const [start, setStart] = useState(false);
    const [coordinates, setCoordinates] = useState([0, 0]); 
    const X0 = useRef(0);
    const Y0 = useRef(0);
    const X1 = useRef(0);
    const Y1 = useRef(0);
    const Xact = useRef(0);
    const slope = useRef(0);
    const coord_for_ball_animationX = useRef(0);
    const coord_for_ball_animationY = useRef(0);
    const [render, setRender] = useState(false);
    const step = useRef(0);
    const colorArrayIndex = useRef(0);
    const [colorArray, setColorArray] = useState([20]);
    const color=useRef(0);
    const prevColor = useRef(0);
    const reverse=useRef(0);



    useEffect(() => {
        const timer = setTimeout(() => {
            handleUseEffect();
        }, 10);
        return () => clearTimeout(timer);
    })

    useEffect(() => {
        if (start) {
            color.current = getColor();
        }
    }, [reverse.current, start])




    function setCoordinatesValues(start) {
        let arr = randomxy(start);
        setCoordinates([...arr]);
    }



    function handleUseEffect() {
        switch (step.current) {
            case 0: {
                X0.current = coordinates[0];
                Y0.current = coordinates[1];
                step.current = 1;
                setCoordinatesValues(start);

                break
            }
            case 1: {
                X1.current = coordinates[0];
                Y1.current = coordinates[1];
                if (X0.current === X1.current) {
                    step.current = 3;
                    setRender(!render);
                    break
                }
                slope.current = (Y1.current - Y0.current) / (X1.current - X0.current);
                step.current = 2;
                Xact.current = X0.current;
                setRender(!render);
                break

            }
            case 2: {
                coord_for_ball_animationX.current = Xact.current;
                coord_for_ball_animationY.current = Math.floor((Xact.current - X0.current) * slope.current) + Y0.current;
                if ((X1.current === X0.current)) {
                    step.current = 3;
                    setRender(!render);
                }
                else
                    if (X1.current > X0.current) {
                        reverse.current = false;
                        Xact.current++
                        if (Xact.current > X1.current) {
                            step.current = 3;
                        }
                    }
                    else {
                        Xact.current--
                        reverse.current = true;
                        if (Xact.current < X1.current) {
                            step.current = 3;
                        }
                    }
                setRender(!render);

                break;
            }
            case 3: {
                X0.current = X1.current;
                Y0.current = Y1.current;
                step.current = 1;
                setCoordinatesValues(start);
                break
            }
            default: {
                break
            }
        }
    }



    function handleClickStart() {
        setColorArray(randomColor());
        setStart(true);
    }

    function handleClickReset() {
        color.current = prevColor.current;
    }



    const getColor = useCallback(() => {
        let bckGrnd = colorArray[colorArrayIndex.current];
        colorArrayIndex.current++;
        console.log("X= ", coord_for_ball_animationX.current);
        console.log("Y= ", coord_for_ball_animationY.current)
        console.log("kolor= ", color);
        prevColor.current = color.current;
        if (colorArrayIndex.current > 20) {
            setColorArray(randomColor());
            colorArrayIndex.current = 0;
            bckGrnd = colorArray[colorArrayIndex.current];
        }
        if ((bckGrnd === "") || (bckGrnd == null)) { bckGrnd = '#FF0000'; }
        return bckGrnd;
    }, [reverse.current, start]);


    return (
        <div className="ball">

            <BallDef
                left={coord_for_ball_animationX.current}
                top={coord_for_ball_animationY.current}
                bck={color.current}
            />

            <button className="start" onClick={handleClickStart}>Start</button>
            <button className="reset" onClick={handleClickReset}>Reset color</button>
        </div>
    )

}

export default Ball;