
export const randomColor = () => {
    // const min=0;
    // const max=65535;
    // let index_outer=0;
    // let index_inner=0;
    let index = 0;

    let colorArr = [20];

    for (index = 0; index < 20; index++) {
        colorArr[index] = randomColor();
    }

    function formatColorString(colorString) {
        if ((colorString.length) < 2) {
            return "0" + colorString;
        }
        return colorString;
    }


    function randomColor() {
        const max = 0xFF;

        // let colorNumb=Math.floor(Math.random() * max);
        // return '#'+colorNumb.toString(16);
        let R = (Math.round(Math.random() * max)).toString(16);
        R = formatColorString(R);
        let G = (Math.round(Math.random() * max)).toString(16);
        G = formatColorString(G);
        let B = (Math.round(Math.random() * max)).toString(16);
        B = formatColorString(B);
        return '#' + R + G + B;
    }



    return colorArr;
}