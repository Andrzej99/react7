import Frame from './components/frame'

function App() {

  return (
    <div>
      <Frame />
    </div>
  );
}

export default App;
